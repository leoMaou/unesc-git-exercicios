Implemente um rota GET para o express chamada /gerador que retorna uma string 
como no exemplo:

### Cássio Ramos é um futebolista brasileiro de 32 anos que atua como goleiro. Atualmente defende o Corinthians.

A informação gerada deverá conter os seguintes requisitos:
- Nome e Sobrenome (aleatórios)
- Idade (17-40 anos)
- Posição (aleatória) 
- Clube (aleatório) 

Para a geração, utilize o arquivo  [gerador_jogador.json](https://gitlab.com/snippets/1896187/raw)