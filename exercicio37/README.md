Implemente uma rota POST no exercício anterior chamada /jogador.
Essa rota deverá receber um objeto JSON no formato abaixo e salvá-lo no arquivo gerador_jogador.json.

- { "clube": "Tubarão" } //adiciona a string tubarão no objeto.clube
- { "nome": "Biro-biro"} //adiciona a string Biro-biro no objeto.nome