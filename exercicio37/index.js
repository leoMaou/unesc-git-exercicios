const express = require('express')
const app = express()
const port = 3000
const request = require('request')

app.get('/', (req, res) => {

    res.send('Digite "localhost:3000/jogador" para executar a função!')

})

app.route('/jogador')

    .get(function (req, res) {
        var fs = require("fs")

        fs.readFile("gerador_jogador.json", "utf8", function (err, data) {
            if (err) {
                return console.log("Erro ao ler arquivo");
            }

            var DataJson = JSON.parse(data)

            var objetoJSON = {
                "nome": DataJson["nome"][Math.floor(Math.random() * DataJson.nome.length)],
                "sobrenome": DataJson["sobrenome"][Math.floor(Math.random() * DataJson.sobrenome.length)],
                "posicao": DataJson["posicao"][Math.floor(Math.random() * DataJson.posicao.length)],
                "clube": DataJson["clube"][Math.floor(Math.random() * DataJson.clube.length)],
                "idade": Math.floor(Math.random() * 24) + 17
            }

            var mascara = function (chave, valor) {
                if (chave == "idade") {
                    if (valor > 16 && valor < 23) {
                        return 'novato'
                    }
                    else if (valor > 22 && valor < 29) {
                        return 'profissional'
                    }
                    else if (valor > 28 && valor < 35) {
                        return 'veterano'
                    }
                    else if (valor > 34 && valor < 41) {
                        return 'master'
                    }
                } else {
                    return valor
                }
            }

            var stringJSON = JSON.stringify(objetoJSON, mascara)

            res.json(JSON.parse(stringJSON))

        })

    })
    .post(function (req, res) {
        var fs = require("fs")

        fs.readFile("gerador_jogador.json", "utf8", function (err, data) {
            if (err) {
                return console.log("Erro ao ler arquivo");
            }

            var objetoJSON = JSON.parse(data)

            objetoJSON["nome"].push("Biro-Biro")
            objetoJSON["clube"].push("Tubarão")

            jsonFile = JSON.stringify(objetoJSON, null, '  ')

            fs.writeFile('gerador_jogador.json', objetoJSON, function (err) {
                if (err) throw err;
                console.log('Salvo!')
            })

            res.json(objetoJSON)
        })
    })

app.listen(port, () => console.log(`Rodando...`))
