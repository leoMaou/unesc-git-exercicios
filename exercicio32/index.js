const express = require('express');
const app = express();

app.use(express.urlencoded());
app.use(express.static('public'));

app.use('/',function (req, res, next) {
    console.log('Passando pelo Middleware');
    console.log(req.url);

    switch (req.url) {
        case '/':
            next();
            break;
        case '/sucesso':
            next();
            break;
        case '/login':
            next();
        case '/404':
            next();
        default:
            res.redirect('/404.html');
    }
});

app.listen(8080, () => {
    console.log('O servidor está online');
});

app.get('/', (req, res) => {
    res.redirect('/login.html');
});

app.post('/login', (req, res) => {
    if(req.body.user == 'root' && req.body.password == 'unesc2019') {
        res.redirect('/sucesso.html');
    }else {
        res.redirect('/login.html?invalid=false');
    }
});