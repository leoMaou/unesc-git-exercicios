/*
Implemente um gerador de números de telefone. 
Para cada número inserido em uma lista na página, 
o usuário deve poder iniciar/encerrar uma ligação (fictícia)
*/

var cont = 0


function gerarNumero() {

    let prefixo = '48 9'
    let numero = ''

    for (let i = 0; i < 8; i++) {
        numero = numero.concat(Math.floor(Math.random() * 10))
    }

    adicionaNumero(prefixo.concat(numero))

}

function adicionaNumero(numero) {

    // Cria e realiza append da div do número
    var divNumero = document.createElement('div')
    divNumero.setAttribute('class', 'row')
    // divNumero.setAttribute("id", cont)

    var telefone = document.createElement('h4')
    telefone.setAttribute('class', 'col-9')
    telefone.textContent = numero

    //Cria botao
    var botaoLigar = document.createElement('input')
    botaoLigar.setAttribute("type", "button")
    botaoLigar.setAttribute("value", "Ligar")
    botaoLigar.setAttribute("id", cont)
    botaoLigar.setAttribute('onClick', 'return ligar(\"' + cont + '\")')
    botaoLigar.setAttribute('class', 'botao btn-info')

    divNumero.appendChild(telefone)
    divNumero.appendChild(botaoLigar)

    document.getElementById('div').appendChild(divNumero)

    cont++
}


function ligar(id) {

    let botaoLigar = document.getElementById(id)
    var comparador = botaoLigar.getAttribute("class");

    // Compara se a ligação está ativa, se sim desliga, se não, liga.
    if (comparador == "botao btn-danger") {

        botaoLigar.setAttribute('class', 'botao btn-info')
        botaoLigar.setAttribute("value", "Ligar")

    } else {

        botaoLigar.setAttribute('class', 'botao btn-danger')
        botaoLigar.setAttribute("value", "Desligar")
    }
}