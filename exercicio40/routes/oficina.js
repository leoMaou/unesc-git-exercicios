const express = require('express')
const router = express.Router()

var fs = require("fs")

fs.readFile("data.json", "utf8", function (err, data) {
    if (err) {
        return console.log("Erro ao ler arquivo");
    }

    var objetoJSON = JSON.parse(data)

    router.get('/', function (req, res) {
        res.json(objetoJSON['oficina'])
    })

    router.get('/:id', function (req, res) {
        if (objetoJSON['oficina'][req.params.id] == null) {
            res.send("Não existe oficina com este ID")
        } else {
            res.json(objetoJSON['oficina'][req.params.id])
        }
    })

    router.post('/', function (req, res) {
        var oficina = req.body
        objetoJSON['oficina'].push(oficina)
        atualizaJSON()
        res.json(oficina)
    })

    router.put('/:id', function (req, res) {
        var oficina = req.body
        if (objetoJSON['oficina'][req.params.id]) {
            objetoJSON['oficina'][req.params.id] = oficina
            atualizaJSON()
        }

        res.json(oficina)

    })

    router.delete('/:id', function (req, res) {
        if (objetoJSON['oficina'][req.params.id] == null) {
            res.json("Não existe oficina com este ID")
        } else {
            var deleteOficina = objetoJSON['oficina'].splice(req.params.id, 1)
            atualizaJSON()
            res.json(deleteOficina)
        }

    })


    function atualizaJSON() {

        const fs = require('fs');

        fs.writeFile("data.json", JSON.stringify(objetoJSON), function (err) {

            if (err) {
                return console.log(err);
            }
        });
    }

})

module.exports = router