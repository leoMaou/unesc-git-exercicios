const express = require('express')
const router = express.Router()

var fs = require("fs")

fs.readFile("data.json", "utf8", function (err, data) {
    if (err) {
        return console.log("Erro ao ler arquivo");
    }

    var objetoJSON = JSON.parse(data)

    var erro = {

        "erro": {
            "msg": "A capacidade máxima foi atingida", "codigo": 400
        }

    }

    router.put('/:id', function (req, res) {
        var oficina = req.body

        if (oficina.participantes.length > oficina.local) {
            res.json(erro)
        } else {
            if (objetoJSON['oficina'][req.params.id]) {
                objetoJSON['oficina'][req.params.id] = oficina
                atualizaJSON()
            }

            res.json(oficina)
        }

    })

    function atualizaJSON() {

        const fs = require('fs');

        fs.writeFile("data.json", JSON.stringify(objetoJSON), function (err) {

            if (err) {
                return console.log(err);
            }
        });
    }

})

module.exports = router