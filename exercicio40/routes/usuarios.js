const express = require('express')
const router = express.Router()

var fs = require("fs")

fs.readFile("data.json", "utf8", function (err, data) {
    if (err) {
        return console.log("Erro ao ler arquivo");
    }

    var objetoJSON = JSON.parse(data)

    router.get('/', function (req, res) {
        res.json(objetoJSON['usuarios'])
    })

    router.get('/:id', function (req, res) {
        if (objetoJSON['usuarios'][req.params.id] == null) {
            res.send("Não existe usuario com este ID")
        } else {
            res.json(objetoJSON['usuarios'][req.params.id])
        }
    })

    router.post('/', function (req, res) {
        var usuarios = req.body
        objetoJSON['usuarios'].push(usuarios)
        atualizaJSON()
        res.json(usuarios)
    })

    router.put('/:id', function (req, res) {
        var usuarios = req.body
        if (objetoJSON['usuarios'][req.params.id]) {
            objetoJSON['usuarios'][req.params.id] = usuarios
            atualizaJSON()
        }

        res.json(local)

    })

    router.delete('/:id', function (req, res) {
        if (objetoJSON['usuarios'][req.params.id] == null) {
            res.json("Não existe usuario com este ID")
        } else {
            var deleteUsuario = objetoJSON['usuarios'].splice(req.params.id, 1)
            atualizaJSON()
            res.json(deleteUsuario)
        }

    })

    function atualizaJSON() {

        const fs = require('fs');

        fs.writeFile("data.json", JSON.stringify(objetoJSON), function (err) {

            if (err) {
                return console.log(err);
            }
        });
    }

})

module.exports = router