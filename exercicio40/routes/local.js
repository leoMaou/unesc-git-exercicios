const express = require('express')
const router = express.Router()

var fs = require("fs")

fs.readFile("data.json", "utf8", function (err, data) {
    if (err) {
        return console.log("Erro ao ler arquivo");
    }

    var objetoJSON = JSON.parse(data)

    router.get('/', function (req, res) {
        res.json(objetoJSON['local'])
    })

    router.get('/:id', function (req, res) {
        if (objetoJSON['local'][req.params.id] == null) {
            res.send("Não existe local com este ID")
        } else {
            res.json(objetoJSON['local'][req.params.id])
        }
    })

    router.post('/', function (req, res) {
        var local = req.body
        objetoJSON['local'].push(local)
        atualizaJSON()
        res.json(local)
    })

    router.put('/:id', function (req, res) {
        var local = req.body
        if (objetoJSON['local'][req.params.id]) {
            objetoJSON['local'][req.params.id] = local
            atualizaJSON()
        }

        res.json(local)

    })

    router.delete('/:id', function (req, res) {
        if (objetoJSON['local'][req.params.id] == null) {
            res.json("Não existe local com este ID")
        } else {
            var deleteLocal = objetoJSON['local'].splice(req.params.id, 1)
            atualizaJSON()
            res.json(deleteLocal)
        }

    })

    function atualizaJSON() {

        const fs = require('fs');

        fs.writeFile("data.json", JSON.stringify(objetoJSON), function (err) {

            if (err) {
                return console.log(err);
            }
        });
    }

})

module.exports = router