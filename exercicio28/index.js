const args = process.argv;
const fs = require('fs');
const level = require('level');
var db = level('PALAVRAS_DB');

console.log('Path: ' + args[2]);

if (args.length > 2) {

    try {
        var path = args[2];
        var file = fs.readFileSync(path, 'utf8');
        if (file) {
           var words = JSON.parse(file);

            console.log("Palavras para salvar");
            console.log(words);

            if(words) {
                for (var i = 0; i < words.length; i++) {
                    console.log("Salvando palavra: " + words[i]);
                    db.put("palavra", words[i], function (err) {
                        console.error('Não foi possivel salvar os valores', err);
                    })
                }
            }
        }
    } catch (e) {
        console.error('\nNão foi possivel ler o arquivo json passado pelo path\n', e);
    }

} else {
    console.error("\nNenhum Path passado por parametro!\n");
}