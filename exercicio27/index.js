const readLine = require('readline-sync');
const fs = require('fs');

var stdin = '';
var words = [];
do {
    stdin = readLine.question('Escolha\n0 - para finalizar\nOu digite uma palavra:\n');

    if (stdin && stdin != 0) {

        try {
            var file = fs.readFileSync('data', 'utf8');
            if (file) {
                words = JSON.parse(file);
            }
        } catch (e) {
            console.error('\nNão foi possivel ler o arquivo data\n')
        }

        if (!words.includes(stdin)) {
            console.log('adicionando palavra: ' + stdin);
            words.push(stdin);
        }

        console.log(words);
        fs.writeFile('data', JSON.stringify(words), {encoding: 'utf8', flag: 'w'}, function (error) {
            if(error != null) {
                console.error('error ao salvar a palavra', error);
            }
        });
    }

} while (stdin != 0);

