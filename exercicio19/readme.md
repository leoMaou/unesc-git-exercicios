git init:
Cria um repostiorio git ou reinicializa um que ja exista

git config --global user.name "turing":
Seta o nome do git global que ira valer para todos os projetos da maquina

git add EXERCICIO.txt:
Adicionar o arquivo EXERCICIO.txt ao index do git

git add . :
Adiciona todos os arquivos na pasta em que o terminal está atualmente ao index do git

git commit -m "Adicionado nova interface":
Gera um commit dos arquivos adicionados no index com mesagem

git commit:
Gera um commit das alterações

git reset --hard HEAD:
Reset current HEAD to the specified state.


cd Downloads:
vai para a pasta Downloads

pwd:
mostra o diretorio atual

cd .. :
volta um diretorio

ls:
mostra os arquivos do diretorio atual

git pull:
Puxa as os commit do repositorio

git push:
envia os commit para o repositorio

git clone https://gitlab.com/rVenson/meurepositorio :
baixa os dados do repositorio, no caso faz um clone como o nome ja diz
