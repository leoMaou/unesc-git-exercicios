const express = require('express');
const app = express();
const usuarios = require('./api/routes/usuario');
const oficinas = require('./api/routes/oficina');
const locais = require('./api/routes/local');


app.use(express.json());

app.use('/usuarios', usuarios);
app.use('/oficinas', oficinas);
app.use('/locais', locais);

app.listen("8080", () => {
    console.log("api iniciada");
});