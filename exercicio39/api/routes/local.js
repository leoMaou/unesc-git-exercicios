const express = require('express');
const router = express.Router();
const fs = require('fs');

//?name=r
router.get('/', (re, res) => {
    var nome = req.query.name;
    var professor = req.query.professor;
    var data;

    try {
        var file = fs.readFileSync('../../data.json', 'utf8');
        if (file) {
            data = JSON.parse(file);
        }
    } catch (e) {
        console.error('\nNão foi possivel ler o arquivo data\n')
        res.status(502).send('Não foi possivel ler o arquivo')
    }

    if (filtro) {
        data.local = data.local.filter(nome => nome.includes(filtro));
    }

    res.json(data.local)
});

router.get("/:id", (req, res) => {
    var localId = req.params.id;
    var data;

    try {
        var file = fs.readFileSync('../../data.json', 'utf8');
        if (file) {
            data = JSON.parse(file);
        }
    } catch (e) {
        console.error('\nNão foi possivel ler o arquivo data\n');
        res.status(502).send('Não foi possivel ler o arquvio');
    }

    if (id) {
        var local = data.local.filter(_id => _id == localId);
    } else {
        res.status(502).send('Não foi especificado nenhum id');
    }

    res.json(local)
});

router.delete("/:id", (req, res) => {
    var localId = req.params.id;
    var data;
    var local;

    try {
        var file = fs.readFileSync('../../data.json', 'utf8');
        if (file) {
            data = JSON.parse(file);
        }
    } catch (e) {
        console.error('\nNão foi possivel ler o arquivo data\n');
        res.status(502).send('Não foi possivel ler o arquvio');
    }

    if (localId) {
        local = data.local.splice(id, localId);

        fs.writeFile('../../data.json', JSON.stringify(data), {encoding: 'utf8', flag: 'w'}, function (error) {
            if (error != null) {
                console.error('error ao salvar dados', error);
                res.status(502).send('Não foi possivel deletar os dados');
            }
        });

    } else {
        res.status(502).send('Não foi especificado nenhum id');
    }

    if (!local) {
        res.status(502).send('Nenhum local deletado');
    }

    res.json(local);
});

router.post("/salvar", (req, res) => {
    var novo_local = req.body.nome;
    var data;

    if(novo_local.id && novo_local.nome) {
        res.status(502).send('Obrigatório setar o id e o nome no body');
    }

    try {
        var file = fs.readFileSync('../../data.json', 'utf8');
        if (file) {
            data = JSON.parse(file);
        }
    } catch (e) {
        console.error('\nNão foi possivel ler o arquivo data\n');
        res.status(502).send('Não foi possivel ler o arquvio');
    }

    if(data) {
        data.local.push(novo_local);

        fs.writeFile('../../data.json', JSON.stringify(data), {encoding: 'utf8', flag: 'w'}, function (error) {
            if (error != null) {
                console.error('error ao salvar dados', error);
                res.status(502).send('Não foi possivel salvar os dados');
            }
        });
    }

    res.json(novo_local);
});

router.post("/atualizar/:id", (req, res) => {
    var id = req.params.id;
    var novo_nome = req.body.nome;
    var novo_local;


    try {
        var file = fs.readFileSync('../../data.json', 'utf8');
        if (file) {
            data = JSON.parse(file);
        }
    } catch (e) {
        console.error('\nNão foi possivel ler o arquivo data\n');
        res.status(502).send('Não foi possivel ler o arquvio');
    }

    data.local.forEach( (item, indice) => {
        if(item[indice].id == id) {
            item[indice] = novo_nome;
            novo_local = item[indice];
        }
    });

    fs.writeFile('../../data.json', JSON.stringify(data), {encoding: 'utf8', flag: 'w'}, function (error) {
        if (error != null) {
            console.error('error ao salvar dados', error);
            res.status(502).send('Não foi possivel salvar os dados');
        }
    });

    res.json(novo_local);
});

module.exports = router;