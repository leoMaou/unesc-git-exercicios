const express = require('express');
const router = express.Router();
const fs = require('fs');

//?name=r
router.get('/', (re, res) => {
    var filtro = req.query.name;
    var data;

    try {
        var file = fs.readFileSync('../../data.json', 'utf8');
        if (file) {
            data = JSON.parse(file);
        }
    } catch (e) {
        console.error('\nNão foi possivel ler o arquivo data\n')
        res.status(502).send('Não foi possivel ler o arquvio')
    }

    if (filtro) {
        data.usuarios = data.usuarios.filter(nome => nome.includes(filtro));
    }

    res.json(data.usuarios)
});

router.get("/:id", (req, res) => {
    var usuarioId = req.params.id;
    var data;

    try {
        var file = fs.readFileSync('../../data.json', 'utf8');
        if (file) {
            data = JSON.parse(file);
        }
    } catch (e) {
        console.error('\nNão foi possivel ler o arquivo data\n');
        res.status(502).send('Não foi possivel ler o arquvio');
    }

    if (id) {
        var usuario = data.usuarios.filter(_id => _id == usuarioId);
    } else {
        res.status(502).send('Não foi especificado nenhum id');
    }

    res.json(usuario)
});

router.delete("/:id", (req, res) => {
    var usuarioId = req.params.id;
    var data;
    var usuario;

    try {
        var file = fs.readFileSync('../../data.json', 'utf8');
        if (file) {
            data = JSON.parse(file);
        }
    } catch (e) {
        console.error('\nNão foi possivel ler o arquivo data\n');
        res.status(502).send('Não foi possivel ler o arquvio');
    }

    if (usuarioId) {
        usuario = data.usuarios.splice(id, usuarioId);

        fs.writeFile('../../data.json', JSON.stringify(data), {encoding: 'utf8', flag: 'w'}, function (error) {
            if (error != null) {
                console.error('error ao salvar dados', error);
                res.status(502).send('Não foi possivel deletar os dados');
            }
        });

    } else {
        res.status(502).send('Não foi especificado nenhum id');
    }

    if (!usuario) {
        res.status(502).send('Nenhum usuario deletado');
    }

    res.json(usuario);
});

router.post("/salvar", (req, res) => {
    var novo_usuario = req.body.nome;
    var data;

    if(novo_usuario.id && novo_usuario.nome) {
        res.status(502).send('Obrigatório setar o id e o nome no body');
    }

    try {
        var file = fs.readFileSync('../../data.json', 'utf8');
        if (file) {
            data = JSON.parse(file);
        }
    } catch (e) {
        console.error('\nNão foi possivel ler o arquivo data\n');
        res.status(502).send('Não foi possivel ler o arquvio');
    }

    if(data) {
        data.usuarios.push(novo_usuario);

        fs.writeFile('../../data.json', JSON.stringify(data), {encoding: 'utf8', flag: 'w'}, function (error) {
            if (error != null) {
                console.error('error ao salvar dados', error);
                res.status(502).send('Não foi possivel salvar os dados');
            }
        });
    }

    res.json(novo_usuario);
});

router.post("/atualizar/:id", (req, res) => {
    var id = req.params.id;
    var novo_nome = req.body.nome;
    var novo_usuario;


    try {
        var file = fs.readFileSync('../../data.json', 'utf8');
        if (file) {
            data = JSON.parse(file);
        }
    } catch (e) {
        console.error('\nNão foi possivel ler o arquivo data\n');
        res.status(502).send('Não foi possivel ler o arquvio');
    }

    data.usuarios.forEach( (item, indice) => {
       if(item[indice].id == id) {
           item[indice] = novo_nome;
           novo_usuario = item[indice];
       }
    });

    fs.writeFile('../../data.json', JSON.stringify(data), {encoding: 'utf8', flag: 'w'}, function (error) {
        if (error != null) {
            console.error('error ao salvar dados', error);
            res.status(502).send('Não foi possivel salvar os dados');
        }
    });

    res.json(novo_usuario);
});

module.exports = router;
