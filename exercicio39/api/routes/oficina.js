const express = require('express');
const router = express.Router();
const fs = require('fs');

//?name=r
router.get('/', (re, res) => {
    var nome = req.query.name;
    var professor = req.query.professor;
    var data;

    try {
        var file = fs.readFileSync('../../data.json', 'utf8');
        if (file) {
            data = JSON.parse(file);
        }
    } catch (e) {
        console.error('\nNão foi possivel ler o arquivo data\n')
        res.status(502).send('Não foi possivel ler o arquivo')
    }

    if (filtro) {
        data.oficina = data.oficina.filter(nome => nome.includes(filtro));
    }

    res.json(data.oficina)
});

router.get("/:id", (req, res) => {
    var oficinaId = req.params.id;
    var data;

    try {
        var file = fs.readFileSync('../../data.json', 'utf8');
        if (file) {
            data = JSON.parse(file);
        }
    } catch (e) {
        console.error('\nNão foi possivel ler o arquivo data\n');
        res.status(502).send('Não foi possivel ler o arquvio');
    }

    if (id) {
        var oficina = data.oficina.filter(_id => _id == oficinaId);
    } else {
        res.status(502).send('Não foi especificado nenhum id');
    }

    res.json(oficina)
});

router.delete("/:id", (req, res) => {
    var oficinaId = req.params.id;
    var data;
    var oficina;

    try {
        var file = fs.readFileSync('../../data.json', 'utf8');
        if (file) {
            data = JSON.parse(file);
        }
    } catch (e) {
        console.error('\nNão foi possivel ler o arquivo data\n');
        res.status(502).send('Não foi possivel ler o arquvio');
    }

    if (oficinaId) {
        oficina = data.oficina.splice(id, oficinaId);

        fs.writeFile('../../data.json', JSON.stringify(data), {encoding: 'utf8', flag: 'w'}, function (error) {
            if (error != null) {
                console.error('error ao salvar dados', error);
                res.status(502).send('Não foi possivel deletar os dados');
            }
        });

    } else {
        res.status(502).send('Não foi especificado nenhum id');
    }

    if (!oficina) {
        res.status(502).send('Nenhum oficina deletado');
    }

    res.json(oficina);
});

router.post("/salvar", (req, res) => {
    var novo_oficina = req.body.nome;
    var data;

    if(novo_oficina.id && novo_oficina.nome) {
        res.status(502).send('Obrigatório setar o id e o nome no body');
    }

    try {
        var file = fs.readFileSync('../../data.json', 'utf8');
        if (file) {
            data = JSON.parse(file);
        }
    } catch (e) {
        console.error('\nNão foi possivel ler o arquivo data\n');
        res.status(502).send('Não foi possivel ler o arquvio');
    }

    if(data) {
        data.oficina.push(novo_oficina);

        fs.writeFile('../../data.json', JSON.stringify(data), {encoding: 'utf8', flag: 'w'}, function (error) {
            if (error != null) {
                console.error('error ao salvar dados', error);
                res.status(502).send('Não foi possivel salvar os dados');
            }
        });
    }

    res.json(novo_oficina);
});

router.post("/atualizar/:id", (req, res) => {
    var id = req.params.id;
    var novo_nome = req.body.nome;
    var novo_oficina;


    try {
        var file = fs.readFileSync('../../data.json', 'utf8');
        if (file) {
            data = JSON.parse(file);
        }
    } catch (e) {
        console.error('\nNão foi possivel ler o arquivo data\n');
        res.status(502).send('Não foi possivel ler o arquvio');
    }

    data.oficina.forEach( (item, indice) => {
        if(item[indice].id == id) {
            item[indice] = novo_nome;
            novo_oficina = item[indice];
        }
    });

    fs.writeFile('../../data.json', JSON.stringify(data), {encoding: 'utf8', flag: 'w'}, function (error) {
        if (error != null) {
            console.error('error ao salvar dados', error);
            res.status(502).send('Não foi possivel salvar os dados');
        }
    });

    res.json(novo_oficina);
});

module.exports = router;