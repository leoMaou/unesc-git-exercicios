var contPokemon = 0

function enviar() {

    //Cria e realiza append da div do pokemon
    var divPokemon = document.createElement('div')
    divPokemon.setAttribute("id", "pokemon" + contPokemon)


    var numero = document.getElementById('numero').value
    var nome = document.getElementById('nome').value
    var tipo = document.getElementById('tipo').value
    var descricao = document.getElementById('descricao').value
    var imagem = document.getElementById('imagem').value
    var lista = {number: numero, name: nome, type: tipo, description: descricao, image: imagem}

    // Cria os dados do pokemon
    var numero = document.createElement('h4')
    var nome = document.createElement('h4')
    var tipo = document.createElement('h4')
    var descricao = document.createElement('h4')
    var imagem = document.createElement('img')
    imagem.setAttribute("src", lista.image);

    //Cria botao
    var botaoDeletar = document.createElement('input')
    botaoDeletar.setAttribute("type", "button")
    botaoDeletar.setAttribute("value", "Deletar")
    var botaoModificar = document.createElement('input')
    botaoModificar.setAttribute("type", "button")
    botaoModificar.setAttribute("value", "Modificar")


    divPokemon.setAttribute('class', 'row')
    numero.setAttribute('class', 'col-2')
    nome.setAttribute('class', 'col-2')
    tipo.setAttribute('class', 'col-2')
    descricao.setAttribute('class', 'col-2')
    imagem.setAttribute('class', 'col-2 img')
    botaoDeletar.setAttribute('class', 'col-1 btn btn-danger botao')
    botaoDeletar.setAttribute('onClick', 'return deletar(\"pokemon' + contPokemon + '\")')
    botaoModificar.setAttribute('class', 'col-1 btn btn-info botao')
    botaoModificar.setAttribute('onClick', 'return modificar(\"pokemon' + contPokemon + '\")')


    numero.textContent = lista.number
    nome.textContent = lista.name
    tipo.textContent = lista.type
    descricao.textContent = lista.description

    divPokemon.appendChild(numero)
    divPokemon.appendChild(nome)
    divPokemon.appendChild(tipo)
    divPokemon.appendChild(descricao)
    divPokemon.appendChild(imagem)
    divPokemon.appendChild(botaoDeletar)
    divPokemon.appendChild(botaoModificar)

    document.getElementById('div').appendChild(divPokemon)

    contPokemon++

    return false;

}

function deletar(id) {
    let divPokemonDelete = document.getElementById(id)
    divPokemonDelete.parentNode.removeChild(divPokemonDelete)

}

function modificar(id) {
    deletar(id)
    enviar()


}