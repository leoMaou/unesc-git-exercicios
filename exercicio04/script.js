var form = document.getElementById('createPlayer');

form.addEventListener('submit', function (e) {
    var name = document.getElementById('name_character').value;

    if(name == 'Admin' || name == 'GM' || name == 'Moderador' ) {
        alert('O nome ' + name + ' Não é permitido');
        e.preventDefault();
    }

    if(name.length < 3) {
        alert('O nome do personagem deve conter pelo menos 3 letras');
        e.preventDefault();
    }

    if(document.getElementById('rbelfo').checked && document.getElementById('rbvenore').checked) {
        alert('É proibido elfos em venore');
        e.preventDefault();
    }

    if(document.getElementById('rbanao').checked && document.getElementById('rbcarlin').checked) {
        alert('É proibido  anões em carlin');
        e.preventDefault();
    }
});