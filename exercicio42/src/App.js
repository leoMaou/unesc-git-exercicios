import React from 'react';
import './App.css';
import Velocidade from'./components/Velocidade'
import Temperatura from'./components/Temperatura'
import ModoFuncionamento from'./components/ModoFuncionamento'

function App() {
  return (<div>
<Velocidade /> 
<Temperatura />
<ModoFuncionamento />

  </div>  
    )}

export default App;
