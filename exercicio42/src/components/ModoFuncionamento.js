import React from 'react'

class Velocidade extends React.Component {

    constructor(props) {
        super(props)
        this.state = { modo: 2, text: "Nenhum modo" }
    }


    alterar = () => {

        switch (this.state.modo) {

            case 1: {
                this.setState({ modo: 2 })
                this.setState({ text: "Aquecer" })
                break
            }
            case 2: {
                this.setState({ modo: 3})
                this.setState({ text: "Ventilar" })
                break
            }
            case 3: {
                this.setState({ modo: 1 })
                this.setState({ text: "Gelo" })
                break

            }
            default:{
                this.setState({ text: "Nenhum modo" })
            }
        }

    }

    render() {
        return (

            <div>Modo: {this.state.text}
                <button onClick={this.alterar} >Alterar</button>
            
            </div>

        )
    }
}

export default Velocidade