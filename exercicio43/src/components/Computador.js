import React from 'react'

class Computador extends React.Component {

    constructor(props) {
        super(props)
        this.state = { computador: this.props.computador }

    }

    render() {
        return <div>hostname: {this.state.computador.hostname} - processador: {this.state.computador.processador} 
        - memoria: {this.state.computador.memoria} - armazenamento: {this.state.computador.armazenamento}
        - estado: {this.state.computador.estado}</div>
    }
}

export default Computador