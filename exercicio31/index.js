const express = require('express');
const app = express();
const fs = require('fs');

app.use(express.urlencoded());
app.use(express.static('html'));
app.use(express.static('products'));

app.use(function (req, res, next) {
    console.log('Passando pelo Middleware');
    next();
});

app.listen(8080, () => {
    console.log('O servidor está online');
});

app.get('/', (req, res) => {
    console.log('requisição get');
    res.end();
});

app.post('/save', (req, res) => {
    console.log('Salvando produto: ' + req.body.produto);

    fs.writeFile('products/data.txt', req.body.produto + '\n', {encoding: 'utf8', flag: 'as'}, function (error) {
        if (error != null) {
            console.error('error ao salvar a palavra', error);
        }
    });

    res.redirect("/");
});
