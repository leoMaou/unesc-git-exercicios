var args = process.argv;

if (args.length > 2) {
    var sum = 0;
    var numberToSum = 0;
    for (var i = 2; i < args.length; i++) {
        numberToSum = parseInt(args[i]);
        if (args[i] % 2 === 0) {
            sum += numberToSum;
        }

    }
    console.log('Somatório: ' + sum);
}
