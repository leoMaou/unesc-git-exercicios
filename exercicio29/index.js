/*Implemente um pacote capaz de realizar requisições para a API
Dicionário Aberto, cujo ponto principal pode ser acessado através do
endereço: http://dicionario-aberto.net/search-json/PALAVRA_DESEJADA .
Utilize requsições http do tipo GET para extrair os dados (não esqueça
de substituir a PALAVRA_DESEJADA ). O script deverá retornar na tela para
o usuário informações sobre a palavra pesquisada , definição e origem 
*/

// USUÁRIO ENTRA COM A PALAVRA A SER PESQUISADA

var rl = require('readline-sync')
var palavra = rl.question("Digite a palavra a ser pesquisada: ")

if (palavra == "") {
  console.warn("\nInforme uma palavra!")
} else {
  requisicao()
}

function requisicao() {

  const request = require('request')
  request('http://dicionario-aberto.net/search-json/' + palavra, function (err, resposta, body) {

    var jsonString = JSON.parse(body)
    console.clear()
    console.log("Palavra pesquisada: " + jsonString['entry']['form']['orth'])
    console.log("Definição: " + jsonString['entry']['sense']['0']['def'])
    console.log("Origem: " + jsonString['entry']['etym']['@orig'])
    console.log("\n")
  })
}